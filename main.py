from apscheduler.schedulers.blocking import BlockingScheduler
from pymarc import Record, Field
from apiclient.discovery import build
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import csv, json, re, difflib, os

API_KEY = "AIzaSyAxs19BvsmeFX4_1lMZuGOGo-hcATNFx0E"
dict = []
global selected
books = build('books', 'v1', developerKey=API_KEY)
logarray = []
sched = BlockingScheduler()
global last_pos


@sched.scheduled_job('cron', day_of_week='mon-sat', hour=17, minute = 54)
def scheduled_job():
	count = 0
	write_last_pos = ""
	selected = []

	with open('lastread.txt', 'r') as read_last_pos:
		last_pos = read_last_pos.readline().strip()		#read last checked entry in the csv file. the id of that entry written in this file.

	f = open('library.csv')
	csv_f = csv.reader(f)

	if last_pos == "":
		print "start from first"
	else:
		#print last_pos
		for row in csv_f:
			if last_pos == row[0]:
				break
			else:
				print "previous results"

	for row in csv_f:
		count = count + 1
		if count > 1000:				#count of values search per round
			break
		else:
			last_pos_to_write = row[0]

			items1 = {}
			items2 = {}
			items3 = {}
			items = {}
			avgratio1 = 0
			avgratio2 = 0
			avgratio3 = 0
			testno = 0
			# search by isbn
			try:
				isbn = row[9]
				if isbn:
					if len(isbn) == 9:
						isbn = "0" + isbn
					else:
						pass
					items1 = books.volumes().list(q = 'isbn:' + isbn).execute().get('items', [])
					if len(items1) > 0:
						items = items1
						testno = 1
						logarray = [row[0], row[1], row[2], row[9], testno]
				else:
					pass
			except:
				pass


			if len(items) <= 0:				#if isbn search gives no result then,
				title = row[1]
				authors = row[2]
				lang = row[6]
				try:
					while re.search(r'(\W+et al\W$|\W+ed\W$|\W+ed$|\W+et al$)', authors, re.I|re.M).group():
						authors = re.sub(r'(\W+et al\W$|\W+ed\W$|\W+ed$|\W+et al$)', "", authors)
				except:
					pass

				try:
					res = re.search(r'(^k$|\Wk\W|MAL)', lang, re.I|re.M).group()
					title = ""
					authors = ""
				except:
					pass

				if (title != "" or authors != ""):
					items2 = books.volumes().list(q = "intitle:" + title + "+inauthor:" + authors).execute().get('items', [])
					if len(items2) <= 0:
						items3 = books.volumes().list(q = authors + " " + title).execute().get('items', [])
						if len(items3) > 0:
							ratio = fuzz.partial_ratio(title, items3[0]['volumeInfo']['title'])
							if ratio > 95:
								items = items3
								testno = 3
								try:
									logarray = [row[0], title, authors, row[9], testno]
								except:
									logarray = [row[0], title, authors, '', testno]
							else:
								logarray = [row[0], title, authors, '', '0']
								with open('log2.csv', 'a') as log2csv:
									writer = csv.writer(log2csv, delimiter=',', quotechar = '"', quoting = csv.QUOTE_MINIMAL)
									writer.writerow(logarray)
								log2 = row
								f = open('log.txt', 'a')
								print >> f, row
								f.close()
						else:
							continue
					else:
						items = items2
						testno = 2
						try:
							logarray = [row[0], title, authors, row[9], testno]
						except:
							logarray = [row[0], title, authors, '', testno]
				else:
					pass

			try:
				bookid = items[0]['id']
				booktitle = items[0]['volumeInfo']['title']
				try:
					description = items[0]['volumeInfo']['description']
				except:
					description = "not given"

				try:
					authors = items[0]['volumeInfo']['authors']
				except:
					authors = "not specified"
	
				authors = ','.join(authors)
				dict = [bookid, booktitle, description, authors]
				#print authors
				selected.append(dict)
			except:
				continue
		
			#log2.csv creating which contains details of books and which test used to get output.
			# 0 - no search gives result		1 - isbn search		2 - title and author specific search 		3 - plain search
			with open('log2.csv', 'a') as log2csv:
				writer = csv.writer(log2csv, delimiter=',', quotechar = '"', quoting = csv.QUOTE_MINIMAL)
				writer.writerow(logarray)


	if os.path.isfile("sample.csv"):
		with open('sample.csv', 'rb') as f:
			reader = csv.reader(f)
			csv_data = list(reader)
			if csv_data != "":
				selected = selected + csv_data


	with open('sample.csv', 'w') as output:
		writer = csv.writer(output, delimiter=',', quotechar = '"', quoting = csv.QUOTE_MINIMAL)
		for item in selected:
			writer.writerow(item)

	with open('file.mrc', 'w') as out_marc:
		for item in selected:
			record = Record()
			record.add_field(
				Field(
					tag = '001', 
					indicators = ['0','1'],
					subfields = [
						'a', item[0],
					]
				),
				Field(
					tag = '100', 
					indicators = ['0','1'],
					subfields = [
						'a', item[3],
					]
				),
				Field(
					tag = '245', 
					indicators = ['0','1'],
					subfields = [
						'a', item[1],
						'c', 'by ' + item[3]
					]
				),
				Field(
					tag = '520',
					indicators = ['0','1'],
					subfields = [
						'a', item[2]
					]
				)
			)
			out_marc.write(record.as_marc())

	with open('lastread.txt', 'w') as write_last_pos:
		print >> write_last_pos, last_pos_to_write


	print "finished"

sched.start()
